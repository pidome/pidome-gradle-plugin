pipeline {
    agent any
    tools {
        gradle 'Gradle-7.2'
        jdk "JDK-16"
    }
    environment {
        JAVA_HOME = "${jdk}"
    }
    stages {
        stage('Initialize') {
            steps {
                script{
                    GRADLE_BUILD_DEFAULT='gradle --configure-on-demand -x check  -PBRANCH_NAME=${BRANCH_NAME} -PENV_BUILD_NUMBER=${BUILD_NUMBER}'
                }
                sh 'echo "JDK path = $JAVA_HOME"'
                sh 'gradle --configure-on-demand clean'
            }
        }
        stage('Code Analysis and tests') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} pidomeCheck"
            }
        }
        stage('Build Module') {
            steps {
                sh "${GRADLE_BUILD_DEFAULT} build"
                archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true, onlyIfSuccessful: true
            }
        }
        stage('Artifact publish'){
            when { 
                anyOf { 
                    branch 'release'; branch 'development' 
                } 
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'nexus_publish', passwordVariable: 'archpass', usernameVariable: 'archuser')]) {
                    sh "${GRADLE_BUILD_DEFAULT} -Parchuser=$archuser -Parchpass=$archpass publish"
                }
            }
        }
        stage('Bitbucket publish') {
            when { 
                anyOf { 
                    branch 'release'; branch 'development' 
                } 
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'bitbucket-cloud', passwordVariable: 'scmpass', usernameVariable: 'scmuser')]) {
                    script {
                        MODULE_BUILD_VERSION = sh (
                            script: 'gradle --configure-on-demand -q properties | grep "version:" | awk \'{print $2}\'',
                            returnStdout: true
                        ).trim()
                    }
                    sh "echo Publishing version: ${MODULE_BUILD_VERSION}"
                    sh "curl --fail-early -u ${scmuser}:${scmpass} -X POST https://api.bitbucket.org/2.0/repositories/pidome/pidome-gradle-plugin/downloads -F files=@build/libs/pidome-gradle-plugin-${MODULE_BUILD_VERSION}.jar"
                }
            }
        }
    }
    post { 
        always {
            jacoco(execPattern: 'build/reports/jacoco/*.exec')
            //junit '**/test-results/test/*.xml'
            recordIssues enabledForFailure: true, tools: [java(), javaDoc()]
            recordIssues enabledForFailure: true, tool: cpd(pattern: 'build/reports/cpd/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: checkStyle(pattern: 'build/reports/checkstyle/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: spotBugs(pattern: 'build/reports/spotbugs/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: pmdParser(pattern: 'build/reports/pmd/main.xml'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            recordIssues enabledForFailure: true, tool: taskScanner(includePattern:'**/*.java', excludePattern:'target/**/*', highTags:'FIXME', normalTags:'TODO'), qualityGates: [[threshold: 1, type: 'TOTAL', unstable: true]]
            jiraSendBuildInfo site: 'pidome.atlassian.net'
        }
        cleanup { 
            cleanWs() 
        }
    }
}