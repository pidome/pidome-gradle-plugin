/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle;

import java.net.URI;
import javax.inject.Inject;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.internal.logging.text.StyledTextOutputFactory;
import org.pidome.platform.plugin.gradle.module.PidomeModuleConfiguration;
import org.pidome.platform.plugin.gradle.module.PidomeModuleConstants;
import org.pidome.platform.plugin.gradle.module.PidomeModuleExtension;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;
import org.pidome.platform.plugin.gradle.module.dependencies.PidomeDependencies;
import org.pidome.platform.plugin.gradle.module.tasks.TaskJavadocJar;
import org.pidome.platform.plugin.gradle.module.tasks.TaskPidomeCheck;
import org.pidome.platform.plugin.gradle.module.tasks.TaskSourceJar;

/**
 * The PiDome gradle plugin to supply module development against pidome.
 * <p>
 * This module extends both the java and the maven plugins to supply normal java
 * development and publication options.
 *
 * @author john.sirach
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize"})
public class PidomeModulePlugin implements Plugin<Project> {

    /**
     * Module configuration.
     */
    private final PidomeModuleConfiguration pidomeModuleConfiguration;

    /**
     * Module dependencies.
     */
    private final PidomeDependencies dependencies;

    /**
     * Constructor.
     *
     * @param outputFactory Factory for styled text output.
     */
    @Inject
    public PidomeModulePlugin(final StyledTextOutputFactory outputFactory) {
        pidomeModuleConfiguration = new PidomeModuleConfiguration(outputFactory);
        dependencies = new PidomeDependencies(outputFactory);
    }

    /**
     * Called by gradle.
     *
     * @param project The project.
     */
    @Override
    public void apply(final Project project) {
        PidomeModuleTools.setProject(project);

        PidomeModuleTools.verifyGradleVersion();
        PidomeModuleTools.verifyJavaVersion();

        PidomeModuleTools.setModuleReleaseType();

        applyBasePlugins(project);

        PidomeModuleExtension extension = createExtension(project);
        createDefaultRepositories(project);
        dependencies.createBomDependency(project, extension);

        createPidomeTasks(project);

        createArtifactsCollection(project);

        pidomeModuleConfiguration.apply(project, extension);
    }

    /**
     * Creates the pidome configuration extension.
     *
     * @param project The project.
     * @return The extension.
     */
    private PidomeModuleExtension createExtension(final Project project) {
        project.getExtensions().create(PidomeModuleConstants.GENERIC_NAME, PidomeModuleExtension.class);
        return project.getExtensions().findByType(PidomeModuleExtension.class);
    }

    /**
     * Apply any base plugins.
     *
     * @param project The project on which to apply.
     */
    private void applyBasePlugins(final Project project) {
        project.getPluginManager().apply("java");
        project.getPluginManager().apply("maven-publish");
        if (!project.getPluginManager().hasPlugin("java")) {
            throw new UnsatisfiedLinkError("'java' plugin not found");
        }
        if (!project.getPluginManager().hasPlugin("maven-publish")) {
            throw new UnsatisfiedLinkError("'maven-publish' plugin not found");
        }
    }

    /**
     * Sets the default repositories to use.
     *
     * @param project The project.
     */
    private void createDefaultRepositories(final Project project) {
        project.getRepositories().mavenLocal();
        project.getRepositories().mavenCentral();
        project.getRepositories().maven(maven -> {
            maven.setName(PidomeModuleConstants.PIDOME_MAVEN_SNAPSHOT_REPOSITORY_NAME);
            maven.setUrl(URI.create(PidomeModuleConstants.PIDOME_MAVEN_SNAPSHOT_REPOSITORY));
            maven.setAllowInsecureProtocol(true);
        });
        project.getRepositories().maven(maven -> {
            maven.setName(PidomeModuleConstants.PIDOME_MAVEN_RELEASE_REPOSITORY_NAME);
            maven.setUrl(URI.create(PidomeModuleConstants.PIDOME_MAVEN_RELEASE_REPOSITORY));
            maven.setAllowInsecureProtocol(true);
        });
    }

    /**
     * Create specific tasks.
     *
     * @param project The project.
     */
    private void createPidomeTasks(final Project project) {
        project.getTasks().register(PidomeModuleConstants.TASK_SOURCEJAR_NAME, TaskSourceJar.class);
        project.getTasks().register(PidomeModuleConstants.TASK_JAVADOCJAR_NAME, TaskJavadocJar.class);
        project.getTasks().register(PidomeModuleConstants.TASK_PIDOME_CHECK, TaskPidomeCheck.class)
                .configure(TaskPidomeCheck::configure);

    }

    /**
     * The artifacts collection.
     *
     * @param project The project.
     */
    private void createArtifactsCollection(final Project project) {
        project.getArtifacts().add(PidomeModuleConstants.CONFIGURATION_ARCHIVES,
                project.getTasks().getByName(PidomeModuleConstants.TASK_SOURCEJAR_NAME));
        project.getArtifacts().add(PidomeModuleConstants.CONFIGURATION_ARCHIVES,
                project.getTasks().getByName(PidomeModuleConstants.TASK_JAVADOCJAR_NAME));
    }

}
