/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import com.github.spotbugs.snom.SpotBugsPlugin;
import de.aaschmid.gradle.plugins.cpd.CpdPlugin;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.gradle.api.Plugin;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.plugins.quality.CheckstylePlugin;
import org.gradle.api.plugins.quality.PmdPlugin;
import org.gradle.testing.jacoco.plugins.JacocoPlugin;

/**
 * The scripts used for the build scripts.
 *
 * @author john.sirach
 */
public enum BuildPlugins {

    /**
     * The Java base plugin.
     */
    JAVA(JavaBasePlugin.class, "java-base", ""),
    /**
     * To implement spotbugs.
     */
    SPOTBUGS(SpotBugsPlugin.class, "com.github.spotbugs-base", "gradle.plugin.com.github.spotbugs.snom:spotbugs-gradle-plugin:4.7.5"),
    /**
     * To implement CPD.
     */
    CPD(CpdPlugin.class, "de.aaschmid.cpd", "de.aaschmid:gradle-cpd-plugin:3.3"),
    /**
     * The jacoco plugin.
     */
    JACOCO(JacocoPlugin.class, "jacoco", ""),
    /**
     * The checkstyle plugin.
     */
    CHECKSTYLE(CheckstylePlugin.class, "checkstyle", ""),
    /**
     * The PMD plugin.
     */
    PMD(PmdPlugin.class, "pmd", "");

    /**
     * The class.
     */
    private final Class<? extends Plugin> clazz;

    /**
     * Plugin id.
     */
    private final String id;

    /**
     * The dependency string.
     */
    private final String dependency;

    /**
     * Constructor.
     *
     * @param theClazz The class.
     * @param pluginId the plugin id.
     * @param dependencyString The dependency string.
     */
    BuildPlugins(final Class<? extends Plugin> theClazz, final String pluginId, final String dependencyString) {
        this.clazz = theClazz;
        this.id = pluginId;
        this.dependency = dependencyString;
    }

    /**
     * Returns the class.
     *
     * @return Returns the class.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "Required for applying plugins in a generic way")
    public Class<? extends Plugin> getClazz() {
        return this.clazz;
    }

    /**
     * Return the plugin name.
     *
     * @return Name of the plugin.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Returns the dependency.
     *
     * @return The dependency string.
     */
    public String getDependency() {
        return this.dependency;
    }

}
