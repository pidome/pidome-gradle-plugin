/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.gradle.api.Action;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.Property;
import org.gradle.api.publish.maven.MavenPomIssueManagement;
import org.gradle.api.publish.maven.MavenPomLicense;
import org.gradle.api.publish.maven.MavenPomLicenseSpec;
import org.gradle.api.publish.maven.MavenPomMailingList;
import org.gradle.api.publish.maven.MavenPomMailingListSpec;
import org.gradle.api.publish.maven.MavenPomOrganization;
import org.gradle.api.publish.maven.MavenPomScm;
import org.gradle.api.publish.maven.internal.publication.DefaultMavenPomLicense;
import org.gradle.internal.reflect.Instantiator;

/**
 * Implementation of PidomeDistribution.
 *
 * @author John Sirach
 */
@SuppressWarnings({"PMD.AvoidFieldNameMatchingMethodName", "PMD.BeanMembersShouldSerialize", "PMD.AvoidDuplicateLiterals"})
public class PidomeDistribution implements MavenPomLicenseSpec, MavenPomMailingListSpec {

    /**
     * Issue management.
     */
    private MavenPomIssueManagement issueManagement;
    /**
     * Project licenses.
     */
    private final List<MavenPomLicense> licenses = new ArrayList<>();
    /**
     * Mailing lists.
     */
    private final List<MavenPomMailingList> mailingLists = new ArrayList<>();
    /**
     * Organization.
     */
    private MavenPomOrganization organization;
    /**
     * SCM.
     */
    private MavenPomScm scm;

    /**
     * If the developer information should be used for organization value.
     */
    private final Property<Boolean> developerSetsOrganization;

    /**
     * If the developer information should be used for organization value.
     */
    private final Property<Boolean> pidomeRepository;

    /**
     * Objectfactory.
     */
    private final ObjectFactory objectFactory;
    /**
     * Instantiator.
     */
    private final Instantiator instantiator;

    /**
     * Constructor.
     *
     * @param factory The object factory for creating members.
     * @param instantiate Plain instantation object.
     */
    @Inject
    @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "It's required")
    public PidomeDistribution(final ObjectFactory factory, final Instantiator instantiate) {
        this.objectFactory = factory;
        this.instantiator = instantiate;
        this.developerSetsOrganization = objectFactory.property(Boolean.class);
        this.pidomeRepository = objectFactory.property(Boolean.class);
    }

    /**
     * Configure issue management.
     *
     * @param configure configuration object.
     */
    public void issueManagement(final Action<? super MavenPomIssueManagement> configure) {
        if (issueManagement == null) {
            issueManagement = objectFactory.newInstance(MavenPomIssueManagement.class);
        }
        configure.execute(issueManagement);
    }

    /**
     * Return issue management object.
     *
     * @return The issue management.
     */
    public MavenPomIssueManagement getIssueManagement() {
        return this.issueManagement;
    }

    /**
     * Configure licenses.
     *
     * @param configure configuration object.
     */
    public void licenses(final Action<? super MavenPomLicenseSpec> configure) {
        configure.execute(this);
    }

    /**
     * Configure single license.
     *
     * @param configure configuration object.
     */
    @Override
    public void license(final Action<? super MavenPomLicense> configure) {
        configureAndAdd(DefaultMavenPomLicense.class, configure, licenses);
    }

    /**
     * Return all licenses list.
     *
     * @return The licenses.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public List<MavenPomLicense> getLicenses() {
        return this.licenses;
    }

    /**
     * Configure mailing lists.
     *
     * @param configure configuration object.
     */
    public void mailingLists(final Action<? super MavenPomMailingListSpec> configure) {
        configure.execute(this);
    }

    /**
     * Configure a single mailinglist.
     *
     * @param configure configuration object.
     */
    @Override
    public void mailingList(final Action<? super MavenPomMailingList> configure) {
        configureAndAdd(MavenPomMailingList.class, configure, mailingLists);
    }

    /**
     * Return mailing lists.
     *
     * @return The mailing lists.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public List<MavenPomMailingList> getMailingLists() {
        return this.mailingLists;
    }

    /**
     * Configure the organization.
     * <p>
     * When <code>developerSetsOrganization</code> is set to true, this
     * organization configuration won't be used in the pom configuration.
     *
     * @param configure configuration object.
     */
    public void organization(final Action<? super MavenPomOrganization> configure) {
        if (organization == null) {
            organization = objectFactory.newInstance(MavenPomOrganization.class);
        }
        configure.execute(organization);
    }

    /**
     * Return the organization.
     * <p>
     * This won't return the organization set on the pidome.develper when
     * <code>developerSetsOrganization</code> is set to true but what is
     * configured within <code>pidome.distribution</code>.
     *
     * @return The organization.
     */
    public MavenPomOrganization getOrganization() {
        return this.organization;
    }

    /**
     * Set the source control repository info.
     *
     * @param configure configuration object.
     */
    public void scm(final Action<? super MavenPomScm> configure) {
        if (scm == null) {
            scm = objectFactory.newInstance(MavenPomScm.class);
        }
        configure.execute(scm);
    }

    /**
     * Return the source control repository info.
     *
     * @return Source control repository info.
     */
    public MavenPomScm getScm() {
        return this.scm;
    }

    /**
     * Set the <code>pidome.developer</code> configured organization info as the
     * organization info.
     * <p>
     * When this is set, but in <code>pidome.developer</code> the developer
     * organization info is not set it will throw a gradle exception during pom
     * configuration.
     * <p>
     * Be aware that if this option is set to true and organization info is set
     * within <code>pidome.distribution</code> it will be overwritten by the
     * pidome.developer organizational setting. Also when setting the
     * organization info wihtin <code>pidome.distribution</code> and call get it
     * will return what is set within <code>pidome.distribution</code>. It is
     * only disregarded during pom configuration.
     * <p>
     * If you want to keep the developer's organization and organization url
     * separated from the <code>pidome.distribution</code> version do not use
     * this setting or set it explicitly to false.
     *
     * @return Boolean property.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<Boolean> getDeveloperSetsOrganization() {
        return this.developerSetsOrganization;
    }

    /**
     * Set if the PiDome repositories should be used to publish artifacts.
     * <p>
     * When this option is set, also repository credentials must be set in the
     * environment. These are only available on the PiDome build server.
     * <p>
     * When this is set to true, and no credentials exist, publish will be done
     * to <code>localMaven()</code>.
     *
     * @return Boolean property.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<Boolean> getPidomeRepository() {
        return this.pidomeRepository;
    }

    /**
     * Internal method for adding to dsl collections.
     *
     * @param <T> The type.
     * @param clazz The class of type T.
     * @param configure configuration object.
     * @param items The items to add to.
     */
    private <T> void configureAndAdd(final Class<? extends T> clazz, final Action<? super T> configure, final List<T> items) {
        T item = instantiator.newInstance(clazz, objectFactory);
        configure.execute(item);
        items.add(item);
    }

}
