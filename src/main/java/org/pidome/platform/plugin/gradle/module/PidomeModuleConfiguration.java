/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.bundling.Jar;
import org.gradle.api.tasks.compile.JavaCompile;
import org.gradle.api.tasks.javadoc.Javadoc;
import org.gradle.api.tasks.testing.Test;
import org.gradle.external.javadoc.StandardJavadocDocletOptions;
import org.gradle.internal.logging.text.StyledTextOutputFactory;
import static org.pidome.platform.plugin.gradle.module.PidomeModuleTools.getJavaPluginExtension;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeCheckstylePlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeCpdPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeJacocoPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomePmdPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeSpotbugsPlugin;
import org.pidome.platform.plugin.gradle.module.tasks.TaskJavadocJar;

/**
 * Applies additional configurations.
 *
 * @author John Sirach
 */
@SuppressWarnings({"PMD.AvoidFieldNameMatchingMethodName", "PMD.BeanMembersShouldSerialize"})
public class PidomeModuleConfiguration {

    /**
     * Pidome pom generation.
     */
    private PidomeModulePublication publication;

    /**
     * The extension.
     */
    private PidomeModuleExtension extension;

    /**
     * Output factory.
     */
    private final StyledTextOutputFactory outputFactory;

    /**
     * Constructor.
     *
     * @param outFactory factory for styling.
     */
    public PidomeModuleConfiguration(final StyledTextOutputFactory outFactory) {
        this.outputFactory = outFactory;
    }

    /**
     * Apply additional configurations.
     *
     * @param project The project to apply on.
     * @param pidomeExtension The configuration extension.
     */
    public final void apply(final Project project, final PidomeModuleExtension pidomeExtension) {
        this.extension = pidomeExtension;
        this.publication = new PidomeModulePublication(project, extension, this.outputFactory);
        applyPlugins(project);
        applyToTest(project);
        project.afterEvaluate((projectRef) -> {
            extension.checkRequiredExtensionProperties(Boolean.FALSE);
            applyToProject(project);
            applyToJavaDoc(project);
            this.publication.create();
        });
    }

    /**
     * Apply generic settings to the project.
     *
     * @param project The project.
     */
    private void applyToProject(final Project project) {
        project.setDescription(this.extension.getName().get());
        project.setGroup(this.extension.getGroup().get());
        project.setVersion(this.extension.getVersion().get());

        if (!Objects.isNull(this.extension.getName())) {
            project.getTasks().withType(Jar.class).forEach(jar -> {
                jar.getArchiveBaseName().set(this.extension.getArchive());
            });
        }

        project.getTasks().withType(JavaCompile.class).forEach(javaCompile -> {
            javaCompile.getOptions().getCompilerArgs().addAll(
                    List.of("-Xlint:unchecked", "-Xlint:deprecation")
            );
        });
    }

    /**
     * Apply the plugins for during build.
     *
     * @param project The project.
     */
    private static void applyPlugins(final Project project) {
        new PidomeSpotbugsPlugin(project).setDefaults();
        new PidomeCpdPlugin(project).setDefaults();
        new PidomeJacocoPlugin(project).setDefaults();
        new PidomeCheckstylePlugin(project).setDefaults();
        new PidomePmdPlugin(project).setDefaults();
    }

    /**
     * Apply options to test.
     *
     * @param project The project.
     */
    private static void applyToTest(final Project project) {
        project.getTasks().withType(Test.class, (test) -> {
            // You know, java 16...
            test.jvmArgs("--illegal-access=permit");
            if (PidomeModuleTools.isModuleLocalBuild()) {
                test.setIgnoreFailures(Boolean.TRUE);
            }
            test.useJUnitPlatform();
        });
    }

    /**
     * Apply options to javadoc generation.
     * <p>
     * This needs to be applied during after evaluate.
     *
     * @param project The project.
     */
    private void applyToJavaDoc(final Project project) {
        Javadoc javadoc = (Javadoc) project.getTasks().findByName(JavaPlugin.JAVADOC_TASK_NAME);
        javadoc.getInputs().property("moduleName", this.extension.getModuleName());
        javadoc.setEnabled(Boolean.TRUE);

        javadoc.setDestinationDir(
                PidomeModuleTools.createDocsLocation(TaskJavadocJar.JAVADOC_LOCATION)
        );

        StandardJavadocDocletOptions options = (StandardJavadocDocletOptions) javadoc.getOptions();
        List<File> classpath = new ArrayList<>();
        classpath.addAll(getJavaPluginExtension()
                .getSourceSets().getByName("main").getRuntimeClasspath().getFiles());
        options.modulePath(classpath);
        options.source(PidomeModuleConstants.JAVA_VERSION.toString());
        options.classpath(Collections.emptyList());
        options.addBooleanOption("html5", Boolean.TRUE);
        options.tags(TaskJavadocJar.TAG_TODO);
    }

}
