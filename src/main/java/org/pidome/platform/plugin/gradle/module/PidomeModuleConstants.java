/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import org.gradle.api.JavaVersion;

/**
 * Constants used within the module.
 *
 * @author john.sirach
 */
public final class PidomeModuleConstants {

    /**
     * Generic used name.
     */
    public static final String GENERIC_NAME = "pidome";

    /**
     * Generic used name.
     */
    public static final String GENERIC_BOM_CONFIGURATION_NAME = "platformBom";

    /**
     * The BOM group id.
     */
    public static final String BOM_MAVEN_GROUP = PidomeModuleTools.getDefaultProperty("pidome-default-bom-group");

    /**
     * The BOM artifact id.
     */
    public static final String BOM_MAVEN_ARTIFACT = PidomeModuleTools.getDefaultProperty("pidome-default-bom-artifact");

    /**
     * The BOM version id.
     */
    public static final String BOM_MAVEN_VERSION = PidomeModuleTools.getDefaultProperty("pidome-default-bom-version");

    /**
     * Location where code analysis is placed.
     */
    public static final String CODE_REPORTS_LOCATION = "reports";

    /**
     * Location where code documentation is placed.
     */
    public static final String CODE_DOCUMENTATION_LOCATION = "documentation";

    /**
     * Java config.
     */
    public static final String JAVA = "java";

    /**
     * Maven publishing config.
     */
    public static final String MAVEN_PUBLISH = "maven-publish";

    /**
     * The lowest java compatibility version.
     */
    public static final JavaVersion JAVA_VERSION = JavaVersion.VERSION_16;

    /**
     * Pidome snapshot repository name.
     */
    public static final String PIDOME_MAVEN_SNAPSHOT_REPOSITORY_NAME = "PidomeSnapshot";

    /**
     * Pidome snapshot repository.
     */
    public static final String PIDOME_MAVEN_SNAPSHOT_REPOSITORY = "http://builder.pidome.org:9000/repository/pidome-snapshots/";

    /**
     * Pidome snapshot repository name.
     */
    public static final String PIDOME_MAVEN_RELEASE_REPOSITORY_NAME = "PidomeRelease";

    /**
     * Pidome release repository.
     */
    public static final String PIDOME_MAVEN_RELEASE_REPOSITORY = "http://builder.pidome.org:9000/repository/pidome-release/";

    /**
     * Name of the sourceJar task.
     */
    public static final String TASK_JAR_NAME = "jar";

    /**
     * Name of the sourceJar task.
     */
    public static final String TASK_SOURCEJAR_NAME = "sourceJar";
    /**
     * Name of the javaDocJar task.
     */
    public static final String TASK_JAVADOCJAR_NAME = "javadocJar";

    /**
     * PiDome verify version task.
     */
    public static final String TASK_PIDOME_CHECK = "pidomeCheck";

    /**
     * Archives configuration name.
     */
    public static final String CONFIGURATION_ARCHIVES = "archives";

    /**
     * Version snapshot string.
     */
    public static final String VERSION_SNAPSHOT = "SNAPSHOT";

    /**
     * Repository user environment var.
     */
    public static final String REPOSITORY_USER = "archuser";

    /**
     * Repository password environment var.
     */
    public static final String REPOSITORY_PASSWORD = "archpass";

    /**
     * Utility class, no constructor public.
     */
    private PidomeModuleConstants() {
    }

}
