/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Objects;
import javax.inject.Inject;
import org.gradle.api.Action;
import org.gradle.api.GradleException;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.model.ObjectFactory;
import org.gradle.api.provider.Property;
import org.gradle.api.publish.maven.MavenPomDeveloper;
import org.gradle.api.publish.maven.MavenPomDeveloperSpec;
import org.gradle.api.publish.maven.MavenPomLicense;
import org.gradle.api.publish.maven.MavenPomLicenseSpec;
import org.gradle.internal.logging.text.StyledTextOutput;
import org.gradle.internal.logging.text.StyledTextOutputFactory;

/**
 * Any options able to be set on the plugin.
 *
 * @author john.sirach
 */
@SuppressWarnings({"PMD.AvoidFieldNameMatchingMethodName", "PMD.BeanMembersShouldSerialize", "PMD.AvoidDuplicateLiterals"})
public class PidomeModuleExtension implements MavenPomDeveloperSpec, MavenPomLicenseSpec {

    /**
     * The bom version.
     */
    private final Property<String> bomVersion;

    /**
     * @return the group.
     */
    private final Property<String> group;

    /**
     * @return the archive.
     */
    private final Property<String> archive;

    /**
     * @return the name.
     */
    private final Property<String> name;

    /**
     * @return the description.
     */
    private final Property<String> description;

    /**
     * @return the moduleName.
     */
    private final Property<String> moduleName;

    /**
     * @return the version.
     */
    private final Property<String> version;

    /**
     * The main developer.
     */
    private MavenPomDeveloper developer;

    /**
     * The main license.
     */
    private MavenPomLicense license;

    /**
     * Pidome dstribution info.
     */
    private PidomeDistribution distribution;

    /**
     * Object factory.
     */
    private final ObjectFactory objectFactory;

    /**
     * Output factory for styling messages.
     */
    private final StyledTextOutputFactory outputFactory;

    /**
     * Constructor.
     *
     * @param createObjectFactory The object factory.
     * @param textOutputFactory Output factory for styled text.
     */
    @Inject
    @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "It's required")
    public PidomeModuleExtension(final ObjectFactory createObjectFactory, final StyledTextOutputFactory textOutputFactory) {
        this.objectFactory = createObjectFactory;
        this.outputFactory = textOutputFactory;
        this.bomVersion = objectFactory.property(String.class);
        this.group = objectFactory.property(String.class);
        this.archive = objectFactory.property(String.class);
        this.name = objectFactory.property(String.class);
        this.description = objectFactory.property(String.class);
        this.moduleName = objectFactory.property(String.class);
        this.version = objectFactory.property(String.class);
    }

    /**
     * @return the bom version.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getBomVersion() {
        return bomVersion;
    }

    /**
     * @return the group.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getGroup() {
        return group;
    }

    /**
     * @return the archive.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getArchive() {
        return archive;
    }

    /**
     * @return the name.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getName() {
        return name;
    }

    /**
     * @return the description.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getDescription() {
        return description;
    }

    /**
     * @return the moduleName.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getModuleName() {
        return moduleName;
    }

    /**
     * @return the version.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP", justification = "It's required")
    public Property<String> getVersion() {
        return version;
    }

    /**
     * Configures main developer.
     *
     * @param configure configuration action.
     */
    @Override
    public void developer(final Action<? super MavenPomDeveloper> configure) {
        if (developer == null) {
            developer = objectFactory.newInstance(MavenPomDeveloper.class);
        }
        configure.execute(developer);
    }

    /**
     * Returns the main developer.
     *
     * @return The main developer.
     */
    public MavenPomDeveloper getDeveloper() {
        return developer;
    }

    /**
     * Configures main license.
     *
     * @param configure configuration action.
     */
    @Override
    public void license(final Action<? super MavenPomLicense> configure) {
        if (license == null) {
            license = objectFactory.newInstance(MavenPomLicense.class);
        }
        configure.execute(license);
    }

    /**
     * Returns the main license.
     *
     * @return The main license.
     */
    public MavenPomLicense getLicense() {
        return license;
    }

    /**
     * Handle distribution configuration.
     *
     * @param configure configure the dsl.
     */
    public void distribution(final Action<? super PidomeDistribution> configure) {
        if (distribution == null) {
            distribution = objectFactory.newInstance(PidomeDistribution.class);
        }
        configure.execute(distribution);
    }

    /**
     * Returns the distribution info.
     *
     * @return The distribution info.
     */
    public PidomeDistribution getDistribution() {
        return this.distribution;
    }

    /**
     * Checks for minimal required properties.
     *
     * @param throwsException if an exception should be thrown instead of a
     * logged message.
     */
    @SuppressWarnings({"PMD.DataflowAnomalyAnalysis", "PMD.CyclomaticComplexity"})
    public void checkRequiredExtensionProperties(final Boolean throwsException) {
        if (!getGroup().isPresent()
                || !getArchive().isPresent()
                || !getName().isPresent()
                || !getDescription().isPresent()
                || !getModuleName().isPresent()
                || !getVersion().isPresent()
                || Objects.isNull(getDeveloper()) || !getDeveloper().getName().isPresent()
                || Objects.isNull(getLicense()) || !getLicense().getName().isPresent()) {
            final String message = """
Not all required properties are set, you will need at minimum:
pidome {
    group       = '%s' // The maven pacakge group
    archive     = '%s' // The maven archive to create
    name        = '%s' // Friendly short name of the project aka 'Device communication module'
    description = '%s' // A longer description of the project aka 'This module communicates with a device so it does something'
    moduleName  = '%s' // Although modularity is not yet implemented, this prepares for when it is
    version     = '%s' // The maven version
    developer { // This is you, see %s for all parameters.
        name = '%s' // Your name
    }
    license { // A license is required, see '%s' for all parameters.
        name = '%s' // License name
    }
} %s
""".formatted(getGroup().get(),
                    getArchive().get(),
                    getName().get(),
                    getDescription().get(),
                    getModuleName().get(),
                    getVersion().get(),
                    "https://docs.gradle.org/7.2/javadoc/org/gradle/api/publish/maven/MavenPomDeveloper.html",
                    !Objects.isNull(getDeveloper()) ? getDeveloper().getName().get() : "",
                    "https://docs.gradle.org/7.2/javadoc/org/gradle/api/publish/maven/MavenPomLicense.html",
                    !Objects.isNull(getLicense()) ? getLicense().getName().get() : "",
                    !throwsException ? "\n\nPublish will fail when not corrected\n" : ""
            );
            if (throwsException) {
                throw new GradleException("Check the pidome{} configuration in your build.gradle file (See configure output)");
            } else {
                StyledTextOutput textOutput = outputFactory.create(PidomeModuleExtension.class, LogLevel.ERROR);
                textOutput.withStyle(StyledTextOutput.Style.Failure)
                        .text(message);
            }
        }
    }

}
