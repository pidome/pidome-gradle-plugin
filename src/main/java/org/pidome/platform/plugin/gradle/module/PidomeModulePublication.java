/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Objects;
import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.gradle.api.artifacts.repositories.MavenArtifactRepository;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.provider.Provider;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.maven.MavenPom;
import org.gradle.api.publish.maven.MavenPublication;
import org.gradle.api.publish.maven.tasks.PublishToMavenRepository;
import org.gradle.internal.logging.text.StyledTextOutput;
import org.gradle.internal.logging.text.StyledTextOutputFactory;
import org.pidome.platform.plugin.gradle.module.tasks.TaskPidomeCheck;

/**
 * Creates the publication.
 *
 * @author John Sirach
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize"})
public class PidomeModulePublication {

    /**
     * The project.
     */
    private final Project project;

    /**
     * The pidome extension.
     */
    private final PidomeModuleExtension extension;

    /**
     * The publishing extension.
     */
    private final PublishingExtension publishing;

    /**
     * The maven publication.
     */
    private final MavenPublication publication;

    /**
     * Output factory.
     */
    private final StyledTextOutputFactory outputFactory;

    /**
     * Constructor.
     *
     * @param pidomeProject The project.
     * @param moduleExtension The pidome extension.
     * @param textOutputFactory Output factory for styled text.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "It's required")
    public PidomeModulePublication(final Project pidomeProject, final PidomeModuleExtension moduleExtension, final StyledTextOutputFactory textOutputFactory) {
        this.project = pidomeProject;
        this.extension = moduleExtension;
        this.outputFactory = textOutputFactory;
        publishing = project.getExtensions().findByType(PublishingExtension.class);
        this.publication = publishing.getPublications().create("PiDome", MavenPublication.class);
        createComponentsAndResolution();
        addAdditionalArchives();
    }

    /**
     * Apply default and additional meta data to pom.
     */
    public void create() {
        if (this.extension.getDistribution().getPidomeRepository().isPresent()
                && Boolean.TRUE.equals(this.extension.getDistribution().getPidomeRepository().get())) {
            setPidomePublishRepositories();
        }
        MavenPom pom = publication.getPom();
        applyPublicationDefaults();
        applyGenericInfo(pom);
        applyDevelopers(pom);
        applyLicense(pom);
        applyScm(pom);
        applyIssueManagement(pom);
        applyMailingLists(pom);
        applyOrganization(pom);

        this.project.getTasks().withType(PublishToMavenRepository.class)
                .forEach(
                        publishTasks -> publishTasks.dependsOn(this.project.getTasks().withType(TaskPidomeCheck.class)
                        )
                );
    }

    /**
     * Create references to components and dependencies.
     */
    private void createComponentsAndResolution() {
        publication.from(
                this.project.getComponents().findByName("java")
        );
        publication.versionMapping(strategy -> {
            strategy.usage("java-api", mappingStrategy -> mappingStrategy.fromResolutionOf("runtimeClasspath"));
            strategy.usage("java-runtime", mappingStrategy -> mappingStrategy.fromResolutionResult());
        });
    }

    /**
     * Adds the additional archives.
     */
    private void addAdditionalArchives() {
        publication.artifact(this.project.getTasks().named(PidomeModuleConstants.TASK_SOURCEJAR_NAME));
        publication.artifact(this.project.getTasks().named(PidomeModuleConstants.TASK_JAVADOCJAR_NAME));
    }

    /**
     * Set defaults on the publication.
     */
    protected void applyPublicationDefaults() {
        publication.setGroupId(this.extension.getGroup().get());
        publication.setArtifactId(this.extension.getArchive().get());
        publication.setVersion(this.extension.getVersion().get());
    }

    /**
     * Sets the publication repositories to PiDome.
     * <p>
     * When there are no credentials set, it will publish to local.
     */
    private void setPidomePublishRepositories() {
        publishing.repositories(handler -> {
            if (PidomeModuleTools.repositoryCredentialsPresent()) {
                MavenArtifactRepository repository
                        = (MavenArtifactRepository) project.getRepositories().getByName(
                                PidomeModuleTools.isSnapshotVersion()
                                ? PidomeModuleConstants.PIDOME_MAVEN_SNAPSHOT_REPOSITORY_NAME
                                : PidomeModuleConstants.PIDOME_MAVEN_RELEASE_REPOSITORY_NAME);
                repository.credentials(credentials -> {
                    credentials.setUsername(project.findProperty(PidomeModuleConstants.REPOSITORY_USER).toString());
                    credentials.setPassword(project.findProperty(PidomeModuleConstants.REPOSITORY_PASSWORD).toString());
                });
                handler.add(repository);
            }
        });
    }

    /**
     * Apply generic pom info.
     *
     * @param mavenPom This publication's maven pom.
     */
    private void applyGenericInfo(final MavenPom mavenPom) {
        mavenPom.getDescription().set(this.extension.getDescription());
        mavenPom.getName().set(this.extension.getName());
    }

    /**
     * Set the developers.
     *
     * @param mavenPom This publication's maven pom.
     */
    private void applyDevelopers(final MavenPom mavenPom) {
        mavenPom.developers(spec -> spec.developer(developer -> {
            if (!Objects.isNull(this.extension.getDeveloper())) {
                developer.getEmail().set(this.extension.getDeveloper().getEmail());
                developer.getId().set(this.extension.getDeveloper().getId());
                developer.getName().set(this.extension.getDeveloper().getName());
                developer.getOrganization().set(this.extension.getDeveloper().getOrganization());
                developer.getOrganizationUrl().set(this.extension.getDeveloper().getOrganizationUrl());
                developer.getProperties().set(this.extension.getDeveloper().getProperties());
                developer.getRoles().set(this.extension.getDeveloper().getRoles());
                developer.getTimezone().set(this.extension.getDeveloper().getTimezone());
                developer.getUrl().set(this.extension.getDeveloper().getUrl());
            }
        }));
    }

    /**
     * Set the licenses.
     *
     * @param mavenPom This publication's maven pom.
     */
    private void applyLicense(final MavenPom mavenPom) {
        this.extension.getDistribution().getLicenses().add(0, this.extension.getLicense());
        this.extension.getDistribution().getLicenses().forEach(pidomeLicense -> {
            if (!Objects.isNull(pidomeLicense)) {
                mavenPom.licenses(spec -> spec.license(license -> {
                    license.getComments().set(pidomeLicense.getComments());
                    license.getDistribution().set(pidomeLicense.getDistribution());
                    license.getName().set(pidomeLicense.getName());
                    license.getUrl().set(pidomeLicense.getUrl());
                }));
            }
        });
    }

    /**
     * Set the SCM.
     *
     * @param mavenPom This publication's maven pom.
     */
    private void applyScm(final MavenPom mavenPom) {
        if (!Objects.isNull(this.extension.getDistribution().getScm())) {
            mavenPom.scm(scm -> {
                scm.getConnection().set(this.extension.getDistribution().getScm().getConnection());
                scm.getDeveloperConnection().set(this.extension.getDistribution().getScm().getDeveloperConnection());
                scm.getTag().set(this.extension.getDistribution().getScm().getTag());
                scm.getUrl().set(this.extension.getDistribution().getScm().getUrl());
            });
        }
    }

    /**
     * Set the issue management.
     *
     * @param mavenPom This publication's maven pom.
     */
    private void applyIssueManagement(final MavenPom mavenPom) {
        if (!Objects.isNull(this.extension.getDistribution().getIssueManagement())) {
            mavenPom.issueManagement(im -> {
                im.getSystem().set(this.extension.getDistribution().getIssueManagement().getSystem());
                im.getUrl().set(this.extension.getDistribution().getIssueManagement().getUrl());
            });
        }
    }

    /**
     * Set the mailing lists.
     *
     * @param mavenPom This publication's maven pom.
     */
    private void applyMailingLists(final MavenPom mavenPom) {
        this.extension.getDistribution().getMailingLists().forEach(list -> {
            mavenPom.mailingLists(spec -> {
                spec.mailingList(pomList -> {
                    pomList.getArchive().set(list.getArchive());
                    pomList.getName().set(list.getName());
                    pomList.getOtherArchives().set(list.getOtherArchives());
                    pomList.getPost().set(list.getPost());
                    pomList.getSubscribe().set(list.getSubscribe());
                    pomList.getUnsubscribe().set(list.getUnsubscribe());
                });
            });
        });
    }

    /**
     * Set the organization.
     *
     * @param mavenPom This publication's maven pom.
     */
    @SuppressWarnings("PMD.CyclomaticComplexity")
    private void applyOrganization(final MavenPom mavenPom) {
        if (this.extension.getDistribution().getDeveloperSetsOrganization().isPresent()
                && Boolean.TRUE.equals(this.extension.getDistribution().getDeveloperSetsOrganization().get())) {
            if (!Objects.isNull(this.extension.getDeveloper()) && this.extension.getDeveloper().getOrganization().isPresent()
                    && this.extension.getDeveloper().getOrganizationUrl()
                            .orElse(this.extension.getDeveloper().getUrl()).isPresent()) {
                setOrganizationInternal(
                        mavenPom,
                        this.extension.getDeveloper().getOrganization(),
                        this.extension.getDeveloper().getOrganizationUrl()
                                .orElse(this.extension.getDeveloper().getUrl())
                );
            } else {
                final String message = """
pidome.distribution.developerSetsOrganization is set to true but essential information is missing. Please set additonally:
pidome {
    developer {
        organization    = '' // The organization name
        url             = '' // The developer url, if this should not be used also add 'organizationUrl'
        organizationUrl = '' // The organization url if developer 'url' should not be used.
    }
}

Build will fail when building on the PiDome build server.
""";
                if (PidomeModuleTools.isModuleLocalBuild()) {
                    StyledTextOutput textOutput = outputFactory.create(PidomeModulePublication.class, LogLevel.ERROR);
                    textOutput.withStyle(StyledTextOutput.Style.Failure)
                            .text(message);
                } else {
                    throw new GradleException(message);
                }
            }
        } else {
            if (!Objects.isNull(this.extension.getDistribution()) && !Objects.isNull(this.extension.getDistribution().getOrganization())) {
                setOrganizationInternal(
                        mavenPom,
                        this.extension.getDistribution().getOrganization().getName(),
                        this.extension.getDistribution().getOrganization().getUrl()
                );
            }
        }
    }

    /**
     * Fill in organization values.
     *
     * @param mavenPom This publication's maven pom.
     * @param name organization name.
     * @param url organization url.
     */
    private void setOrganizationInternal(final MavenPom mavenPom, final Provider<String> name, final Provider<String> url) {
        mavenPom.organization(org -> {
            org.getName().set(name);
            org.getUrl().set(url);
        });
    }

}
