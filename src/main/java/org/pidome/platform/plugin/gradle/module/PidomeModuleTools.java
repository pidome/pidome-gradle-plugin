/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import groovy.lang.Closure;
import groovy.lang.GroovyShell;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import org.gradle.api.GradleException;
import org.gradle.api.JavaVersion;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.util.GradleVersion;

/**
 * Useful methods.
 *
 * @author John Sirach
 */
public final class PidomeModuleTools {

    /**
     * The project.
     */
    private static Project project;

    /**
     * Minimal supported gradle version.
     */
    private static final GradleVersion SUPPORTED_VERSION = GradleVersion.version("7.0");

    /**
     * Local build name.
     */
    private static final String LOCAL_BUILD = "local";

    /**
     * The default properties.
     */
    private static Properties defaults;
    /**
     * The properties file name.
     */
    private static final String DEFAULT_PROPERTIES_NAME = "org/pidome/plugins/gradle/plugin-default.properties";

    /**
     * Utility class, no public constructor.
     */
    private PidomeModuleTools() {
    }

    /**
     * Set the project for referring to other methods.
     * <p>
     * This must be called first.
     *
     * @param pidomeProject The project reference.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_STATIC_REP2", justification = "It's required")
    public static void setProject(final Project pidomeProject) {
        PidomeModuleTools.project = pidomeProject;
    }

    /**
     * Returns the sourceset of main.
     *
     * @return The sourceset of main.
     */
    public static Set<File> getJavaMainSourceSet() {
        return getJavaPluginExtension()
                .getSourceSets().getByName("main").getAllJava().getSrcDirs();
    }

    /**
     * Returns the sourceset of main.
     *
     * @return The sourceset of main.
     */
    public static SourceSet getJavaMainSourceSetAsSourceSet() {
        return getJavaPluginExtension()
                .getSourceSets().getByName("main");
    }

    /**
     * Returns the report analysis path or file for given target in the build
     * directory.
     *
     * @param target The target which can be a folder or file.
     * @return The analysis path.
     */
    public static File createReportLocation(final String target) {
        return createReportLocation(target, false);
    }

    /**
     * Returns the report analysis path or file for given target in the build
     * directory.
     * <p>
     * This method can create a directory if it doens't exist, make sure you're
     * only do this when a directory is intended.
     *
     * @param target The target which can be a folder or file.
     * @param createDir Create the directory if it doesn't exist.
     * @return The analysis path.
     */
    public static File createReportLocation(final String target, final boolean createDir) {
        File file = project.file(
                new StringBuilder(getProjectBuildDir())
                        .append(File.separator)
                        .append(PidomeModuleConstants.CODE_REPORTS_LOCATION)
                        .append(File.separator)
                        .append(target).toString());
        if (createDir) {
            createDir(file);
        }
        return file;
    }

    /**
     * Returns the documentation path or file for given target in the build
     * directory.
     *
     * @param target The target which can be a folder or file.
     * @return The analysis path.
     */
    public static File createDocsLocation(final String target) {
        return createDocsLocation(target, false);
    }

    /**
     * Returns the documentation path or file for given target in the build
     * directory.
     * <p>
     * This method can create a directory if it doens't exist, make sure you're
     * only do this when a directory is intended.
     *
     * @param target The target which can be a folder or file.
     * @param createDir Create the directory if it doesn't exist.
     * @return The analysis path.
     */
    public static File createDocsLocation(final String target, final boolean createDir) {
        File file = project.file(
                new StringBuilder(getProjectBuildDir())
                        .append(File.separator)
                        .append(PidomeModuleConstants.CODE_DOCUMENTATION_LOCATION)
                        .append(File.separator)
                        .append(target).toString());
        if (createDir) {
            createDir(file);
        }
        return file;
    }

    /**
     * Get project build path.
     *
     * @return The build path.
     */
    public static String getProjectBuildDir() {
        return project.getBuildDir().getPath();
    }

    /**
     * Returns the java plugin configuration extension.
     *
     * @return The extension.
     */
    public static JavaPluginExtension getJavaPluginExtension() {
        return project.getExtensions().findByType(JavaPluginExtension.class);
    }

    /**
     * Verify Gradle version.
     */
    public static void verifyGradleVersion() {
        GradleVersion version = GradleVersion.current();
        if (version.compareTo(SUPPORTED_VERSION) < 0) {
            throw new IllegalArgumentException(String.format(
                    "Gradle version %s is unsupported, version %s or later is required.",
                    version, SUPPORTED_VERSION));
        }
    }

    /**
     * Verify the java version used.
     */
    public static void verifyJavaVersion() {
        if (JavaVersion.current() != PidomeModuleConstants.JAVA_VERSION) {
            throw new GradleException(
                    String.format("The java version used %s, expected is version %s.",
                            JavaVersion.current(),
                            PidomeModuleConstants.JAVA_VERSION)
            );
        }
    }

    /**
     * Set's the release type.
     */
    public static void setModuleReleaseType() {
        project.getExtensions().getExtraProperties().set("releaseType",
                project.hasProperty("BRANCH_NAME") ? project.findProperty("BRANCH_NAME") : "local");
    }

    /**
     * If repository credentials are present.
     *
     * @return If repository credentials are present.
     */
    public static boolean repositoryCredentialsPresent() {
        return project.hasProperty(PidomeModuleConstants.REPOSITORY_USER)
                && project.hasProperty(PidomeModuleConstants.REPOSITORY_PASSWORD);
    }

    /**
     * If current version is snapshot.
     *
     * @return If snapshot.
     */
    public static boolean isSnapshotVersion() {
        return project.getExtensions().findByType(PidomeModuleExtension.class)
                .getVersion().get().endsWith(PidomeModuleConstants.VERSION_SNAPSHOT);
    }

    /**
     * The module's release type.
     *
     * @return The release type.
     */
    public static String getModuleReleaseType() {
        return (String) project.getExtensions().getExtraProperties().get("releaseType");
    }

    /**
     * If the build has a repository branch or not.
     *
     * @return If a repository branch variable has been set.
     */
    public static boolean isModuleLocalBuild() {
        return LOCAL_BUILD.equals(getModuleReleaseType());
    }

    /**
     * Returns the value given by property name from plugin-default.properties.
     *
     * @param propertyName The property name to read.
     * @return The property value.
     */
    public static String getDefaultProperty(final String propertyName) {
        if (Objects.isNull(defaults)) {
            try ( InputStream inputStream = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream(DEFAULT_PROPERTIES_NAME)) {
                defaults = new Properties();
                if (inputStream != null) {
                    defaults.load(inputStream);
                } else {
                    throw new FileNotFoundException(
                            "property file '" + DEFAULT_PROPERTIES_NAME + "' not found in the classpath"
                    );
                }
            } catch (IOException ex) {
                throw new GradleException("Unable to load defaults", ex);
            }
        }
        return defaults.getProperty(propertyName, "");
    }

    /**
     * Simple closure build.
     *
     * @param strings The strings to eval.
     * @return The closure.
     */
    public static Closure<?> buildClosure(final String... strings) {
        String scriptText = "{ script -> " + String.join("\n", strings) + " }";
        return (Closure<?>) new GroovyShell().evaluate(scriptText);
    }

    /**
     * Creates a directory.
     * <p>
     * Throws a gradle exception when failing.
     *
     * @param location The location to create.
     */
    private static void createDir(final File location) {
        if (!location.exists()) {
            if (!location.mkdirs()) {
                throw new GradleException("Could not create directory '" + location.getAbsolutePath() + "'");
            }
        }
    }
}
