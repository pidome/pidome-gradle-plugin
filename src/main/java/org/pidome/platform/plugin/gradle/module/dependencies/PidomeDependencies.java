/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.dependencies;

import java.util.concurrent.TimeUnit;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.DependencySet;
import org.gradle.api.internal.artifacts.result.DefaultResolvedDependencyResult;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.provider.Property;
import org.gradle.internal.logging.text.StyledTextOutput;
import org.gradle.internal.logging.text.StyledTextOutputFactory;
import org.pidome.platform.plugin.gradle.module.PidomeModuleConstants;
import static org.pidome.platform.plugin.gradle.module.PidomeModuleConstants.BOM_MAVEN_ARTIFACT;
import static org.pidome.platform.plugin.gradle.module.PidomeModuleConstants.BOM_MAVEN_GROUP;
import org.pidome.platform.plugin.gradle.module.PidomeModuleExtension;

/**
 * Sets pidome dependencies.
 *
 * @author John Sirach
 * @todo Make the BOM decide what to load where, we don't want to publish a new
 * plugin with every new BOM.
 */
public class PidomeDependencies {

    /**
     * Dependencies store.
     */
    private static final PidomeDependenciesStore STORE = new PidomeDependenciesStore();

    /**
     * Styled text output.
     */
    private final StyledTextOutput textOutPut;

    /**
     * Constructor.
     *
     * @param textOutFactory Output factory for styled text.
     */
    public PidomeDependencies(final StyledTextOutputFactory textOutFactory) {
        textOutPut = textOutFactory.create(PidomeDependencies.class);
    }

    /**
     * Loads dependencies from the BOM for the give extension.
     *
     * @param project The project.
     * @param extension The PiDome extension.
     */
    public void createBomDependency(final Project project, final PidomeModuleExtension extension) {
        Configuration configuration
                = project.getConfigurations()
                        .create(PidomeModuleConstants.GENERIC_BOM_CONFIGURATION_NAME)
                        .setDescription("Configuration to load BOM")
                        .setVisible(true)
                        .setTransitive(true);
        configuration.defaultDependencies((dependencySet) -> {
            Dependency platform = project.getDependencies().enforcedPlatform(
                    getBomCoordinates(extension.getBomVersion())
            );
            platform.because("Supplying expected libraries for pidome module development.");
            dependencySet.add(platform);
        });

        project.getGradle().getTaskGraph().whenReady(graph -> {
            if (!extension.getBomVersion().isPresent()) {
                project.getConfigurations().forEach(config -> {
                    configuration.getResolutionStrategy().cacheChangingModulesFor(1, TimeUnit.MINUTES);
                    configuration.getResolutionStrategy().cacheDynamicVersionsFor(1, TimeUnit.MINUTES);
                });
                configuration.getIncoming().getResolutionResult().getAllDependencies().forEach(dependency -> {
                    if (dependency instanceof DefaultResolvedDependencyResult result) {
                        textOutPut.style(StyledTextOutput.Style.Error)
                                .formatln("""

Parameter pidome.bomVersion not configured, resolved BOM to latest version '%s' by fallback rule '%s'
                                          """,
                                        result.getSelected().getModuleVersion().getVersion(),
                                        PidomeModuleConstants.BOM_MAVEN_VERSION);
                    }
                });
            }
        });

        project.getConfigurations().getByName(JavaPlugin.COMPILE_ONLY_CONFIGURATION_NAME).extendsFrom(configuration);
        addDependencies(project, JavaPlugin.COMPILE_ONLY_CONFIGURATION_NAME);

        project.getConfigurations().getByName(JavaPlugin.TEST_IMPLEMENTATION_CONFIGURATION_NAME).extendsFrom(configuration);
        addDependencies(project, JavaPlugin.TEST_IMPLEMENTATION_CONFIGURATION_NAME);

        project.getConfigurations().getByName(JavaPlugin.TEST_RUNTIME_ONLY_CONFIGURATION_NAME).extendsFrom(configuration);
        addDependencies(project, JavaPlugin.TEST_RUNTIME_ONLY_CONFIGURATION_NAME);

        project.getConfigurations().getByName(JavaPlugin.ANNOTATION_PROCESSOR_CONFIGURATION_NAME).extendsFrom(configuration);
        addDependencies(project, JavaPlugin.ANNOTATION_PROCESSOR_CONFIGURATION_NAME);

    }

    /**
     * Returns the bom version as a dependency string.
     *
     * @param version The version to use.
     * @return The BOM path.
     */
    private String getBomCoordinates(final Property<String> version) {
        return new StringBuilder()
                .append(BOM_MAVEN_GROUP)
                .append(":").append(BOM_MAVEN_ARTIFACT)
                .append(":").append(version.isPresent() ? version.get() : PidomeModuleConstants.BOM_MAVEN_VERSION)
                .toString();
    }

    /**
     * Adds a dependency based on given gradle java plugin configuration.
     *
     * @param project The project.
     * @param configuration The gradle java plugin configuration.
     */
    @SuppressWarnings("PMD.DataflowAnomalyAnalysis")
    private static void addDependencies(final Project project, final String configuration) {
        Configuration config = project.getConfigurations().getByName(configuration);
        DependencySet dependencySet = config.getDependencies();
        STORE.allForConfiguration(configuration).stream()
                .forEach(dependencyString
                        -> dependencySet.add(project.getDependencies().create(dependencyString))
                );
    }

}
