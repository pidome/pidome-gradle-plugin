/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.dependencies;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.gradle.api.plugins.JavaPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeSpotbugsPlugin;

/**
 * Collection of dependencies.
 *
 * @author John Sirach
 */
public class PidomeDependenciesStore {

    /**
     * Store containing all test dependencies.
     */
    private static final Map<String, List<String>> REQUIRED_STORE = new ConcurrentHashMap<>();

    static {
        REQUIRED_STORE.put(JavaPlugin.TEST_RUNTIME_ONLY_CONFIGURATION_NAME,
                List.of("org.junit.jupiter:junit-jupiter-engine"));
        REQUIRED_STORE.put(JavaPlugin.TEST_IMPLEMENTATION_CONFIGURATION_NAME,
                List.of("org.hamcrest:hamcrest",
                        "org.junit.jupiter:junit-jupiter-api",
                        "io.vertx:vertx-junit5",
                        "org.mockito:mockito-junit-jupiter",
                        "org.apache.logging.log4j:log4j-api",
                        "io.vertx:vertx-core",
                        "com.fasterxml.jackson.core:jackson-core",
                        "com.fasterxml.jackson.core:jackson-annotations",
                        "com.fasterxml.jackson.core:jackson-databind",
                        "com.fasterxml.jackson.datatype:jackson-datatype-jdk8",
                        "com.fasterxml.jackson.datatype:jackson-datatype-jsr310",
                        "org.pidome.platform:pidome-modules",
                        "org.pidome.platform:pidome-hardware",
                        "org.pidome.platform:pidome-presentation",
                        "org.pidome.platform:pidome-tools",
                        "org.pidome.platform:pidome-annotations"));

        REQUIRED_STORE.put(JavaPlugin.COMPILE_ONLY_CONFIGURATION_NAME,
                List.of("org.apache.logging.log4j:log4j-api",
                        "io.vertx:vertx-core",
                        "com.fasterxml.jackson.core:jackson-core",
                        "com.fasterxml.jackson.core:jackson-annotations",
                        "com.fasterxml.jackson.core:jackson-databind",
                        "com.fasterxml.jackson.datatype:jackson-datatype-jdk8",
                        "com.fasterxml.jackson.datatype:jackson-datatype-jsr310",
                        "com.github.spotbugs:spotbugs-annotations:" + PidomeSpotbugsPlugin.ANNOTATIONS_VERSION));

        REQUIRED_STORE.put(JavaPlugin.ANNOTATION_PROCESSOR_CONFIGURATION_NAME,
                List.of("org.pidome.platform:pidome-annotations"));

    }

    /**
     * Return each dependency.
     *
     * @param configuration The configuration to return dependencies for.
     * @return All dependencies.
     */
    public List<String> allForConfiguration(final String configuration) {
        return REQUIRED_STORE.get(configuration);
    }

}
