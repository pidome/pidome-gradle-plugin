/**
 * Dependency configuration.
 * <p>
 * Classes required for dependency resolution of used plugins, runtime, tests
 * and PiDome BOM.
 *
 * @since 1.0.0
 * @author John Sirach
 */
package org.pidome.platform.plugin.gradle.module.dependencies;
