/**
 * Module classes.
 * <p>
 * All required classes for the plugin to work.
 *
 * @since 1.0.0
 * @author John Sirach
 */
package org.pidome.platform.plugin.gradle.module;
