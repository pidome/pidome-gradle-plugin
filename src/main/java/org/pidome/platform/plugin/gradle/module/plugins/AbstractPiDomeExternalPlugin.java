/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.plugins;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.AppliedPlugin;
import org.gradle.api.plugins.PluginManager;
import org.pidome.platform.plugin.gradle.module.BuildPlugins;

/**
 * Default abstract for external plugins assigned.
 *
 * @author John Sirach
 * @param <T> The plugin.
 */
public abstract class AbstractPiDomeExternalPlugin<T extends Plugin> {

    /**
     * The project.
     */
    private final Project project;

    /**
     * Load and apply a plugin.
     *
     * @param pidomeProject The project to apply to.
     * @param buildPlugin The plugin to apply.
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP2", justification = "It's required")
    public AbstractPiDomeExternalPlugin(final Project pidomeProject, final BuildPlugins buildPlugin) {
        this.project = pidomeProject;
        project.getPlugins().apply(buildPlugin.getClazz());
        PluginManager manager = project.getPluginManager();
        AppliedPlugin plugin = manager.findPlugin(buildPlugin.getId());
        if (plugin == null) {
            throw new UnsatisfiedLinkError(buildPlugin.getId() + " plugin not found");
        }
    }

    /**
     * Returns the project.
     *
     * @return The project.
     */
    protected final Project getProject() {
        return this.project;
    }

    /**
     * Set defaults on a plugin.
     */
    public abstract void setDefaults();

}
