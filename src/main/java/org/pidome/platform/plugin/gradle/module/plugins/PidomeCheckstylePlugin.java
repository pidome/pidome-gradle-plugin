/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.plugins;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.apache.tools.ant.filters.ReplaceTokens;
import org.apache.tools.ant.filters.ReplaceTokens.Token;
import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.gradle.api.plugins.quality.Checkstyle;
import org.gradle.api.plugins.quality.CheckstyleExtension;
import org.gradle.api.reporting.CustomizableHtmlReport;
import org.gradle.api.reporting.SingleFileReport;
import org.gradle.api.resources.TextResource;
import org.pidome.platform.plugin.gradle.module.BuildPlugins;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;

/**
 * Checkstyle config.
 *
 * @author John Sirach
 */
public class PidomeCheckstylePlugin extends AbstractPiDomeExternalPlugin {

    /**
     * Tool version used.
     */
    public static final String TOOL_VERSION = PidomeModuleTools.getDefaultProperty("pidome-default-toolversion-checkstyle");

    /**
     * Checkstyle analysis folder.
     */
    public static final String REPORT_FOLDER = "checkstyle";

    /**
     * Task name to execute.
     */
    public static final String TASK_NAME = "checkstyleMain";

    /**
     * Configuration file.
     */
    private static final String CONFIG_XML = "org/pidome/plugins/gradle/module/plugins/checkstyle.xml";
    ///**
    // * XSL file.
    // *
    // * Not used, needs correction first.
    // */
    // private static final String TRANFORM_XSL = "org/pidome/plugins/gradle/module/plugins/checkstyle.xsl";
    /**
     * Suppression file.
     */
    private static final String SUPPRESSIONS_XML = "org/pidome/plugins/gradle/module/plugins/checkstyle_suppressions.xml";

    /**
     * Token used in the checkstyle configuration to be replaced with current
     * build dir.
     */
    private static final String BUILD_DIR_TOKEN_REPLACE = "CHECKSTYLE_BUILD_DIR";
    /**
     * XML report name.
     */
    private static final String XML_REPORT = "xml";
    /**
     * XML report file name.
     */
    public static final String XML_REPORT_NAME = "checkstyle.xml";
    /**
     * HTML report name.
     */
    private static final String HTML_REPORT = "html";
    /**
     * HTML report file name.
     */
    private static final String HTML_REPORT_NAME = "checkstyle.html";

    /**
     * Constructor.
     *
     * @param project The project.
     */
    public PidomeCheckstylePlugin(final Project project) {
        super(project, BuildPlugins.CHECKSTYLE);
    }

    /**
     * @@inheritDoc
     */
    @Override
    @SuppressWarnings("CPD-START")
    public void setDefaults() {
        CheckstyleExtension extension = this.getProject().getExtensions()
                .findByType(CheckstyleExtension.class);
        extension.setToolVersion(TOOL_VERSION);
        extension.setSourceSets(List.of(PidomeModuleTools.getJavaMainSourceSetAsSourceSet()));
        extension.setConfig(getConfigWithReplacedBuildDir());
        extension.setReportsDir(PidomeModuleTools.createReportLocation(REPORT_FOLDER, true));
        try {
            extension.getConfigProperties().put(
                    "suppressionFile",
                    Thread.currentThread().getContextClassLoader().getResource(SUPPRESSIONS_XML).toURI()
            );
        } catch (URISyntaxException ex) {
            throw new GradleException("Unable to read suppression file", ex);
        }
        extension.setIgnoreFailures(Boolean.TRUE);

        this.getProject().getTasks().withType(Checkstyle.class)
                .forEach(task -> {
                    task.getReports().forEach(report -> {
                        SingleFileReport xmlReport = (SingleFileReport) task.getReports().findByName(XML_REPORT);
                        xmlReport.getRequired().set(Boolean.TRUE);
                        xmlReport.getOutputLocation().set(
                                PidomeModuleTools.createReportLocation(REPORT_FOLDER + File.separator + XML_REPORT_NAME)
                        );

                        if (PidomeModuleTools.isModuleLocalBuild()) {
                            CustomizableHtmlReport htmlReport = (CustomizableHtmlReport) task.getReports().findByName(HTML_REPORT);
                            //htmlReport.setStylesheet(this.getProject().getResources().getText().fromUri(
                            //        PidomeCheckstylePlugin.class.getClassLoader().getResource(TRANFORM_XSL)
                            //));
                            htmlReport.getRequired().set(Boolean.TRUE);
                            htmlReport.getOutputLocation().set(
                                    PidomeModuleTools.createReportLocation(REPORT_FOLDER + File.separator + HTML_REPORT_NAME)
                            );
                            //task.doFirst((closure) -> {
                            //    PidomeModuleTools.createReportLocation(REPORT_FOLDER + File.separator + "html", true);
                            //});
                        }
                    });
                });

    }

    /**
     * Set safe build root path in checstyle configuration.
     * <p>
     * Checkstyle uses a full path in the report, it is logical this is not
     * expected or wanted. This method replaces the build root folder so files
     * are relative instead of absolute.
     *
     * @return configuration with correct build dir.
     */
    private TextResource getConfigWithReplacedBuildDir() {

        try ( Reader reader = new InputStreamReader(
                new BufferedInputStream(
                        Thread.currentThread().getContextClassLoader().getResource(CONFIG_XML).openStream()
                ), Charset.forName(StandardCharsets.UTF_8.name()));  ReplaceTokens replaceTokens = new ReplaceTokens(reader)) {

            final ReplaceTokens.Token token = new Token();
            token.setKey(BUILD_DIR_TOKEN_REPLACE);
            token.setValue(getProject().getRootDir().getAbsolutePath());
            replaceTokens.addConfiguredToken(token);

            StringBuilder returnConfig = new StringBuilder();

            int data = replaceTokens.read();
            while (data != -1) {
                returnConfig.append((char) data);
                data = replaceTokens.read();
            }
            return this.getProject().getResources().getText().fromString(returnConfig.toString());
        } catch (IOException ex) {
            throw new GradleException("Unable to read checkstyle config file checkstyle.xml", ex);
        }

    }

}
