/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.plugins;

import de.aaschmid.gradle.plugins.cpd.Cpd;
import de.aaschmid.gradle.plugins.cpd.CpdExtension;
import java.io.File;
import org.gradle.api.Project;
import org.gradle.api.reporting.SingleFileReport;
import org.pidome.platform.plugin.gradle.module.BuildPlugins;
import org.pidome.platform.plugin.gradle.module.PidomeModuleConstants;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;

/**
 *
 * @author John Sirach
 */
public class PidomeCpdPlugin extends AbstractPiDomeExternalPlugin {

    /**
     * Tool version used.
     */
    public static final String TOOL_VERSION = PidomeModuleTools.getDefaultProperty("pidome-default-toolversion-cpd");

    /**
     * CPD report folder.
     */
    public static final String REPORT_FOLDER = "cpd";

    /**
     * Task name to execute.
     */
    public static final String TASK_NAME = "cpdCheck";

    /**
     * Report file.
     */
    public static final String REPORT_FILE = REPORT_FOLDER + File.separator + "report.xml";

    /**
     * XML report name.
     */
    private static final String XML_REPORT = "xml";

    /**
     * Constructor.
     *
     * @param project The project.
     */
    public PidomeCpdPlugin(final Project project) {
        super(project, BuildPlugins.CPD);
    }

    /**
     * @@inheritDoc
     */
    @Override
    public void setDefaults() {
        CpdExtension extension = this.getProject().getExtensions()
                .findByType(CpdExtension.class);
        extension.setLanguage(PidomeModuleConstants.JAVA);
        extension.setSkipLexicalErrors(true);
        extension.setToolVersion(TOOL_VERSION);
        extension.setReportsDir(PidomeModuleTools.createReportLocation(REPORT_FOLDER));

        this.getProject().getTasks().withType(Cpd.class)
                .forEach(task -> {
                    task.setIgnoreFailures(true);
                    task.setSource(PidomeModuleTools.getJavaMainSourceSet());
                    SingleFileReport report = task.getReports().findByName(XML_REPORT);
                    report.getRequired().set(Boolean.TRUE);
                    report.getOutputLocation().set(
                            PidomeModuleTools.createReportLocation(REPORT_FILE)
                    );
                });
    }

}
