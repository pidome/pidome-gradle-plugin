/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.plugins;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import org.gradle.api.Project;
import org.gradle.api.reporting.DirectoryReport;
import org.gradle.api.reporting.SingleFileReport;
import org.gradle.api.tasks.testing.Test;
import org.gradle.testing.jacoco.plugins.JacocoPluginExtension;
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension;
import org.gradle.testing.jacoco.tasks.JacocoCoverageVerification;
import org.gradle.testing.jacoco.tasks.JacocoReport;
import org.gradle.testing.jacoco.tasks.rules.JacocoLimit;
import org.gradle.testing.jacoco.tasks.rules.JacocoViolationRule;
import org.pidome.platform.plugin.gradle.module.BuildPlugins;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;

/**
 * The jacoco plugin.
 *
 * @author John Sirach
 */
public class PidomeJacocoPlugin extends AbstractPiDomeExternalPlugin {

    /**
     * Tool version used.
     */
    public static final String TOOL_VERSION = PidomeModuleTools.getDefaultProperty("pidome-default-toolversion-jacoco");

    /**
     * Jacoco report folder.
     */
    public static final String REPORT_FOLDER = "jacoco";

    /**
     * Verification task name.
     */
    public static final String TASK_NAME_TEST_VERIFICATION = "jacocoTestCoverageVerification";

    /**
     * Test report task name.
     */
    public static final String TASK_NAME_TEST_REPORT = "jacocoTestReport";

    /**
     * XML report name.
     */
    private static final String XML_REPORT = "xml";

    /**
     * CSV report name.
     */
    private static final String CSV_REPORT = "csv";

    /**
     * HTML report name.
     */
    private static final String HTML_REPORT = "html";

    /**
     * The file name for merging all reports.
     */
    private static final String MERGE_FILE_NAME = REPORT_FOLDER + File.separator + "jacocoTest.exec";

    /**
     * HTML report name.
     */
    private static final String HTML_REPORT_OUTPUT = REPORT_FOLDER + File.separator + "jacocoHtml";

    /**
     * XML report name.
     */
    private static final String XML_REPORT_OUTPUT = REPORT_FOLDER + File.separator + "jacocoTest.xml";

    /**
     * Name of the test verification task.
     */
    private static final String TASK_TEST_COVERAGE_VERIFICATION_NAME = "jacocoTestCoverageVerification";

    /**
     * Constructor.
     *
     * @param project The project.
     */
    public PidomeJacocoPlugin(final Project project) {
        super(project, BuildPlugins.JACOCO);
    }

    /**
     * @@inheritDoc
     */
    @Override
    public void setDefaults() {
        Test testTask = (Test) getProject().getTasks().findByName("test");

        JacocoPluginExtension extension = this.getProject().getExtensions()
                .findByType(JacocoPluginExtension.class);
        extension.setToolVersion(TOOL_VERSION);
        extension.getReportsDirectory().set(PidomeModuleTools.createReportLocation(REPORT_FOLDER));

        this.getProject().getTasks().forEach(task -> {
            JacocoTaskExtension taskExtension = (JacocoTaskExtension) task.getExtensions().findByName(JacocoPluginExtension.TASK_EXTENSION_NAME);
            if (!Objects.isNull(taskExtension)) {
                taskExtension.setDestinationFile(PidomeModuleTools.createReportLocation(MERGE_FILE_NAME));
            }
        });
        this.getProject().getTasks().withType(JacocoReport.class)
                .forEach(task -> {
                    task.setEnabled(Boolean.TRUE);

                    SingleFileReport xmlReport = (SingleFileReport) task.getReports().findByName(XML_REPORT);
                    xmlReport.getRequired().set(Boolean.TRUE);
                    xmlReport.getOutputLocation().set(
                            PidomeModuleTools.createReportLocation(XML_REPORT_OUTPUT)
                    );
                    task.getReports().findByName(CSV_REPORT).getRequired().set(Boolean.FALSE);
                    if (PidomeModuleTools.isModuleLocalBuild()) {
                        DirectoryReport htmlReport = (DirectoryReport) task.getReports().findByName(HTML_REPORT);
                        htmlReport.getRequired().set(Boolean.TRUE);
                        htmlReport.getOutputLocation().set(
                                PidomeModuleTools.createReportLocation(HTML_REPORT_OUTPUT)
                        );
                    }

                    task.dependsOn(testTask);
                    testTask.finalizedBy(task);

                });
        JacocoCoverageVerification testCoverageTask = (JacocoCoverageVerification) this.getProject()
                .getTasks().findByName(TASK_TEST_COVERAGE_VERIFICATION_NAME);

        testCoverageTask.sourceSets(PidomeModuleTools.getJavaMainSourceSetAsSourceSet());
        testCoverageTask.setEnabled(Boolean.TRUE);
        testCoverageTask.getViolationRules().rule((JacocoViolationRule coveredRatio) -> {
            coveredRatio.limit((JacocoLimit coveredRatioLimit) -> {
                coveredRatioLimit.setCounter("LINE");
                coveredRatioLimit.setValue("COVEREDRATIO");
                coveredRatioLimit.setMinimum(BigDecimal.ZERO);
            });
            coveredRatio.setExcludes(List.of("**/*Test*.class"));
        });

    }

}
