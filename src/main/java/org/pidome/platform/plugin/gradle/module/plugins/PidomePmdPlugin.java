/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.plugins;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import org.gradle.api.GradleException;
import org.gradle.api.Project;
import org.gradle.api.plugins.quality.Pmd;
import org.gradle.api.plugins.quality.PmdExtension;
import org.gradle.api.reporting.SingleFileReport;
import org.gradle.api.reporting.internal.TaskGeneratedSingleFileReport;
import org.pidome.platform.plugin.gradle.module.BuildPlugins;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;

/**
 *
 * @author John Sirach
 */
public class PidomePmdPlugin extends AbstractPiDomeExternalPlugin {

    /**
     * Tool version used.
     */
    public static final String TOOL_VERSION = PidomeModuleTools.getDefaultProperty("pidome-default-toolversion-pmd");

    /**
     * Checkstyle analysis folder.
     */
    public static final String REPORT_FOLDER = "pmd";

    /**
     * Task name to execute.
     */
    public static final String TASK_NAME = "pmdMain";

    /**
     * Configuration file.
     */
    private static final String CONFIG_XML = "org/pidome/plugins/gradle/module/plugins/pmd.xml";
    /**
     * XML report name.
     */
    private static final String XML_REPORT = "xml";
    /**
     * XML report file name.
     */
    public static final String XML_REPORT_NAME = "pmd.xml";
    /**
     * HTML report name.
     */
    private static final String HTML_REPORT = "html";
    /**
     * HTML report file name.
     */
    private static final String HTML_REPORT_NAME = "pmd.html";

    /**
     * Constructor.
     *
     * @param project The project.
     */
    public PidomePmdPlugin(final Project project) {
        super(project, BuildPlugins.PMD);
    }

    /**
     * @@inheritDoc
     */
    @Override
    @SuppressWarnings("CPD-START")
    public void setDefaults() {
        PmdExtension extension = this.getProject().getExtensions()
                .findByType(PmdExtension.class);
        extension.setToolVersion(TOOL_VERSION);
        extension.setSourceSets(List.of(PidomeModuleTools.getJavaMainSourceSetAsSourceSet()));
        extension.setRuleSets(Collections.emptyList());
        try {
            extension.setRuleSetConfig(
                    this.getProject().getResources().getText().fromUri(
                            Thread.currentThread().getContextClassLoader().getResource(CONFIG_XML).toURI()
                    )
            );
        } catch (URISyntaxException ex) {
            throw new GradleException("Could not open PMD rules file", ex);
        }
        extension.setIgnoreFailures(Boolean.TRUE);
        extension.setReportsDir(PidomeModuleTools.createReportLocation(REPORT_FOLDER));

        this.getProject().getTasks().withType(Pmd.class)
                .forEach(task -> {
                    task.getReports().forEach(report -> {
                        SingleFileReport xmlReport = (SingleFileReport) task.getReports().findByName(XML_REPORT);
                        xmlReport.getRequired().set(Boolean.TRUE);
                        xmlReport.getOutputLocation().set(
                                PidomeModuleTools.createReportLocation(REPORT_FOLDER + File.separator + XML_REPORT_NAME)
                        );

                        if (PidomeModuleTools.isModuleLocalBuild()) {
                            TaskGeneratedSingleFileReport htmlReport = (TaskGeneratedSingleFileReport) task.getReports().findByName(HTML_REPORT);
                            htmlReport.getRequired().set(Boolean.TRUE);
                            htmlReport.getOutputLocation().set(
                                    PidomeModuleTools.createReportLocation(REPORT_FOLDER + File.separator + HTML_REPORT_NAME)
                            );
                        }
                    });
                });

    }

}
