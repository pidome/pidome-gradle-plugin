/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.plugins;

import com.github.spotbugs.snom.Effort;
import com.github.spotbugs.snom.SpotBugsExtension;
import com.github.spotbugs.snom.SpotBugsTask;
import java.io.File;
import org.gradle.api.Project;
import org.gradle.api.resources.TextResource;
import org.pidome.platform.plugin.gradle.module.BuildPlugins;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;

/**
 * Applies and defaults for the spotbugs plugin.
 *
 * @author John Sirach
 */
public class PidomeSpotbugsPlugin extends AbstractPiDomeExternalPlugin {

    /**
     * Tool version used.
     */
    public static final String TOOL_VERSION = PidomeModuleTools.getDefaultProperty("pidome-default-toolversion-spotbugs");

    /**
     * Supply the spotbugs annotations version.
     */
    public static final String ANNOTATIONS_VERSION = "4.4.1";

    /**
     * Task name to execute.
     */
    public static final String TASK_NAME = "spotbugsMain";

    /**
     * Effort applied.
     */
    public static final Effort EFFORT = Effort.MAX;
    /**
     * Ignore failures or not.
     */
    public static final boolean IGNORE_FAILURES = true;
    /**
     * Spotbugs analysis folder.
     */
    public static final String REPORT_FOLDER = "spotbugs";

    /**
     * Report file.
     */
    public static final String REPORT_FILE = REPORT_FOLDER + File.separator + "main.xml";

    /**
     * The report xsl.
     */
    private static final String REPORT_XSL = "org/pidome/plugins/gradle/module/plugins/spotbugs.xsl";

    /**
     * Constructor.
     *
     * @param project The project.
     */
    public PidomeSpotbugsPlugin(final Project project) {
        super(project, BuildPlugins.SPOTBUGS);
    }

    /**
     * @@inheritDoc
     */
    @Override
    public void setDefaults() {
        final SpotBugsExtension extension = this.getProject().getExtensions()
                .findByType(SpotBugsExtension.class);
        extension.getEffort().set(EFFORT);
        extension.getIgnoreFailures().set(IGNORE_FAILURES);
        extension.getShowStackTraces().set(Boolean.FALSE);
        extension.getReportsDir().set(PidomeModuleTools.createReportLocation(REPORT_FOLDER));

        final TextResource xslResource = this.getProject().getResources().getText().fromUri(
                Thread.currentThread().getContextClassLoader().getResource(REPORT_XSL)
        );

        this.getProject().getTasks().withType(SpotBugsTask.class)
                .forEach(task -> {
                    task.setSourceDirs(PidomeModuleTools.getJavaMainSourceSetAsSourceSet().getAllJava());
                    task.getReports().forEach(report -> {
                        report.setStylesheet(xslResource);
                        report.setEnabled(true);
                        report.getOutputLocation().set(
                                PidomeModuleTools.createReportLocation(REPORT_FILE)
                        );
                    });
                });
    }

}
