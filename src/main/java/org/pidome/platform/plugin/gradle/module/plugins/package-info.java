/**
 * Plugins configuration.
 * <p>
 * Per plugin configurations.
 *
 * @since 1.0.0
 * @author John Sirach
 */
package org.pidome.platform.plugin.gradle.module.plugins;
