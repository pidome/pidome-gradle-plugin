/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.tasks;

import java.util.List;
import javax.inject.Inject;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.plugins.BasePlugin;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.bundling.Jar;

/**
 * Create JavaDoc jar.
 *
 * @author john.sirach
 */
public class TaskJavadocJar extends Jar {

    /**
     * The javadoc location.
     */
    public static final String JAVADOC_LOCATION = "javadoc";

    /**
     * Allows a todo in the documentation.
     */
    public static final String TAG_TODO = "todo:a:\"To Do:\"";

    /**
     * Constructor.
     *
     * @param projectInternal The project.
     */
    @Inject
    public TaskJavadocJar(final ProjectInternal projectInternal) {
        this.setDependsOn(List.of(JavaPlugin.JAVADOC_TASK_NAME));
        this.setDescription("Create javadoc JAR file");
        this.setGroup(BasePlugin.BUILD_GROUP);
        this.from(projectInternal.getTasks().findByName(JavaPlugin.JAVADOC_TASK_NAME));
        this.getArchiveClassifier().set("javadoc");
    }
}
