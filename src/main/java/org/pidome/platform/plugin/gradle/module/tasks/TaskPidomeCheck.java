/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.tasks;

import groovy.xml.XmlSlurper;
import groovy.xml.slurpersupport.GPathResult;
import groovy.xml.slurpersupport.Node;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;
import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.Task;
import org.gradle.api.logging.LogLevel;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.resources.TextResource;
import org.gradle.api.tasks.TaskAction;
import org.gradle.internal.logging.text.StyledTextOutput;
import org.gradle.internal.logging.text.StyledTextOutputFactory;
import org.gradle.work.DisableCachingByDefault;
import org.pidome.platform.plugin.gradle.module.PidomeModuleExtension;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeCheckstylePlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeCpdPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeJacocoPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomePmdPlugin;
import org.pidome.platform.plugin.gradle.module.plugins.PidomeSpotbugsPlugin;
import org.xml.sax.SAXException;

/**
 * Simple task to execute all verifications configured.
 *
 * @author John Sirach
 */
@SuppressWarnings({"PMD.BeanMembersShouldSerialize"})
@DisableCachingByDefault(because = "Errors should always be checked, build cache is disabled on the build server anyway.")
public class TaskPidomeCheck extends DefaultTask {

    /**
     * Output factory for styling messages.
     */
    private final StyledTextOutput textOutput;

    /**
     * The PiDome configuration extension.
     */
    private final PidomeModuleExtension pidomeExtension;

    /**
     * Constructor.
     *
     * @param textOutputFactory injected for colored output.
     */
    @Inject
    public TaskPidomeCheck(final StyledTextOutputFactory textOutputFactory) {
        textOutput = textOutputFactory.create(TaskPidomeCheck.class, LogLevel.ERROR);
        pidomeExtension = this.getProject().getExtensions().getByType(PidomeModuleExtension.class);
    }

    /**
     * Amount of checkstyle errors found.
     */
    private int checkstyleErrors = 0;

    /**
     * Amount of cpd errors found.
     */
    private int cpdErrors = 0;

    /**
     * Amount of pmd errors found.
     */
    private int pmdErrors = 0;

    /**
     * Amount of spotbugs errors found.
     */
    private int spotbugsErrors = 0;

    /**
     * The task action.
     */
    @TaskAction
    public void execute() {
        checkHasCheckStyleErrors();
        checkHasCpdErrors();
        checkHasPmdErrors();
        checkHasSpotbugsErrors();

        if (checkstyleErrors > 0
                || cpdErrors > 0
                || pmdErrors > 0
                || spotbugsErrors > 0) {
            String message = """
One or more check tasks have errors, please correct before continuing
Checkstyle: %s
CPD       : %s
PMD       : %s
Spotbugs  : %s

Check the reports at '%s'%s
"""
                    .formatted(checkstyleErrors,
                            cpdErrors,
                            pmdErrors,
                            spotbugsErrors,
                            PidomeModuleTools.createReportLocation(""),
                            PidomeModuleTools.isModuleLocalBuild()
                            ? """


This message is a warning, This task will fail when running on the PiDome build server if error count for each is > 0.
                            """
                            : "");
            if (PidomeModuleTools.isModuleLocalBuild()) {
                textOutput.withStyle(StyledTextOutput.Style.Failure)
                        .text(message);
            } else {
                throw new GradleException(message);
            }
        }
    }

    /**
     * Check if there are checkstyle errors.
     *
     * @return true when errors are found
     */
    private boolean checkHasCheckStyleErrors() {
        try {
            TextResource checkstyleReport = getProject().getResources().getText().fromFile(
                    PidomeModuleTools.createReportLocation(PidomeCheckstylePlugin.REPORT_FOLDER + File.separator + PidomeCheckstylePlugin.XML_REPORT_NAME)
            );
            GPathResult rootNode = new XmlSlurper().parseText(checkstyleReport.asString());
            checkstyleErrors = rootNode.children().children().findAll(
                    PidomeModuleTools.buildClosure("{node -> node.name() == 'error'}")
            ).size();
            return checkstyleErrors > 0;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new GradleException("Unable to read Checkstyle report, failing task", ex);
        }
    }

    /**
     * Check if there are cpd errors.
     *
     * @return true when errors are found
     */
    private boolean checkHasCpdErrors() {
        try {
            TextResource pmdReport = getProject().getResources().getText().fromFile(
                    PidomeModuleTools.createReportLocation(PidomeCpdPlugin.REPORT_FILE)
            );
            GPathResult rootNode = new XmlSlurper().parseText(pmdReport.asString());
            cpdErrors = rootNode.children().findAll(
                    PidomeModuleTools.buildClosure("{node -> node.name() == 'duplication'}")
            ).size();
            return cpdErrors > 0;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new GradleException("Unable to read CPD report, failing task", ex);
        }
    }

    /**
     * Check if there are pmd errors.
     *
     * @return true when errors are found
     */
    private boolean checkHasPmdErrors() {
        try {
            TextResource pmdReport = getProject().getResources().getText().fromFile(
                    PidomeModuleTools.createReportLocation(PidomePmdPlugin.REPORT_FOLDER + File.separator + PidomePmdPlugin.XML_REPORT_NAME)
            );
            GPathResult rootNode = new XmlSlurper().parseText(pmdReport.asString());
            pmdErrors = rootNode.children().children().findAll(
                    PidomeModuleTools.buildClosure("{node -> node.name() == 'violation'}")
            ).size();
            return pmdErrors > 0;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new GradleException("Unable to read PMD report, failing task", ex);
        }
    }

    /**
     * Check if there are spotbugs errors.
     *
     * @return true when errors are found
     */
    private boolean checkHasSpotbugsErrors() {
        try {
            TextResource spotbugsReport = getProject().getResources().getText().fromFile(
                    PidomeModuleTools.createReportLocation(PidomeSpotbugsPlugin.REPORT_FILE)
            );
            GPathResult rootNode = new XmlSlurper().parseText(spotbugsReport.asString());
            spotbugsErrors = identifyNodeCount(rootNode.findAll(
                    PidomeModuleTools.buildClosure("{node -> node.name() == 'BugInstance'}")
            ), "BugInstance");
            return spotbugsErrors > 0;
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new GradleException("Unable to read CPD report, failing task", ex);
        }
    }

    /**
     * Configure this task.
     */
    public void configure() {
        this.dependsOn(List.of(
                PidomeCheckstylePlugin.TASK_NAME,
                PidomeSpotbugsPlugin.TASK_NAME,
                PidomePmdPlugin.TASK_NAME,
                PidomeCpdPlugin.TASK_NAME,
                JavaPlugin.TEST_TASK_NAME,
                PidomeJacocoPlugin.TASK_NAME_TEST_REPORT,
                PidomeJacocoPlugin.TASK_NAME_TEST_VERIFICATION));
        this.getOutputs().upToDateWhen(task -> false);
        this.setGroup(JavaBasePlugin.VERIFICATION_GROUP);
        this.doFirst(new Action<Task>() {
            @Override
            public void execute(final Task task) {
                pidomeExtension.checkRequiredExtensionProperties(Boolean.TRUE);
                if (!Objects.isNull(pidomeExtension.getDistribution())
                        && Boolean.TRUE.equals(
                                pidomeExtension.getDistribution().getPidomeRepository().getOrElse(Boolean.FALSE)
                        )
                        && !PidomeModuleTools.repositoryCredentialsPresent()) {
                    textOutput.withStyle(StyledTextOutput.Style.Failure)
                            .text("No credentials to publish to PiDome repository, "
                                    + "publish will be done to local maven repository");
                }
            }
        });
    }

    /**
     * Returns a count of nodes by the given name.
     *
     * @param nodeSet The node set.
     * @param nodeName The name to find.
     * @return The amount found.
     */
    @SuppressWarnings("unchecked")
    private static int identifyNodeCount(final GPathResult nodeSet, final String nodeName) {
        Iterator<Node> iter = nodeSet.childNodes();
        int counter = 0;
        while (iter.hasNext()) {
            if (nodeName.equals(iter.next().name())) {
                counter++;
            }
        }
        return counter;
    }

}
