/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.plugin.gradle.module.tasks;

import java.util.List;
import javax.inject.Inject;
import org.gradle.api.plugins.BasePlugin;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.plugins.JavaPluginExtension;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.bundling.Jar;
import org.pidome.platform.plugin.gradle.module.PidomeModuleTools;

/**
 * Create sources jar.
 *
 * @author john.sirach
 */
public class TaskSourceJar extends Jar {

    /**
     * Constructor.
     */
    @Inject
    public TaskSourceJar() {
        final JavaPluginExtension javaPluginExtension = PidomeModuleTools.getJavaPluginExtension();
        this.setDependsOn(List.of(JavaPlugin.TEST_CLASSES_TASK_NAME));
        this.setDescription("Create sources JAR file");
        this.setGroup(BasePlugin.BUILD_GROUP);
        this.from(javaPluginExtension.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME).getOutput());
        this.getArchiveClassifier().set("sources");
    }

}
