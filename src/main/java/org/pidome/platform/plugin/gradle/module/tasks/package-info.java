/**
 * Module tasks.
 * <p>
 * PiDome module registered tasks.
 *
 * @since 1.0.0
 * @author John Sirach
 */
package org.pidome.platform.plugin.gradle.module.tasks;
