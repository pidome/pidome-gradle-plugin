/**
 * Base package for the module.
 * <p>
 * The module.
 *
 * @since 1.0.0
 * @author John Sirach
 */
package org.pidome.platform.plugin.gradle;
